import React, { Component } from 'react';
import logo from '../../images/logo.svg';
import favicon from '../../images/favicon.ico';
import '../../css/App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
        </header>
        <div className="container">
          <div className="jumbotron">
          </div>
        </div>
      </div>
    );
  }
}

export default App;
