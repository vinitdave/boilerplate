Welcome to your React-Redux, Webpack and Node.js boilerplate project.

## Running the server

1) Configure the config.js in ~/server/config directory. Specifically if you plan to use any MongoDB URL to configuration parameters.

2) You can launch the app from the Terminal:

    $ npm start
    
3) You can build the react-redux app from the Terminal:

    $ npm build-dev or npm build-prod to build in development or production setup