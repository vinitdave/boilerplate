var express = require('express');
var app = express();
var router = express.Router();
var path = require('path');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var session = require('express-session');
var compression = require('compression');
var bunyan = require('bunyan');

var config = require('./config/config');
var logConfig = {
  name: 'sample',
  streams: [{
    level: 'debug',
    type: 'rotating-file',
    path: './server/logs/sample.debug.log',
    period: '1d',
    count: 5
  }]
};
var log = bunyan.createLogger(logConfig);

mongoose.Promise = require('bluebird');
mongoose.set('useFindAndModify', false);
if(config.db!= null)
    mongoose.connect(config.db, { promiseLibrary: require('bluebird'), useNewUrlParser: true })
      .then(() => log.debug('Database Connection Succesful'))
      .catch((err) => log.debug(err));
else
    console.error("Please configure the Mongo DB Connection URL/String in the db parameter in the ~/server/config/config.js before running the application");

//app.set('views', path.join(__dirname, '../client'));
app.use(express.static(path.join(__dirname, '../client/dist')));
app.use(session({secret: 'myvoiceismypassword',resave: false,saveUninitialized: false}));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
app.use(compression());

require('./routes/routes')(router, log);
app.use(router);

module.exports = app;
