module.exports = function(router, log) {

    var sampleRoute = router.route('/api/sample');

    sampleRoute.get(function(req, res) {
        res.sendStatus(200);
    });
};
