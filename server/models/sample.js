var mongoose = require('mongoose');

// Define our Sample schema
var SampleSchema = new mongoose.Schema({
  c_PhoneNumber: {type: String },
  c_FirstName: { type: String },
  c_LastName: { type: String },
  c_StreetAddress: { type: String },
  c_City: { type: String },
  c_State: { type: String },
  c_ZipCode: { type: String },
});

// Export the Mongoose model
module.exports = mongoose.model('Sample', SampleSchema);